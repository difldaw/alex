<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function getIndex(){
        return view('welcome');
    }
    public function getAbout(){
        
        $companyname = "Alejandro Rosas";
        $isUserRegistered = false;
        
        $users = array("Renato", "Erik", "John", "Samatha");
        
        return view('pages.about')
            ->with("companyname", $companyname)
            ->with("isUserRegistered", $isUserRegistered)
            ->with("users", $users);
    }
    
    public function getContact(){
        return view('pages.contact');
    }
    
    public function getDashboard(){
        return 'dashboard';
    }
    public function getReports(){
        return 'reports';
    }
}