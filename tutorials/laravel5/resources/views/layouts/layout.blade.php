<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>
    </head>
</html>
<body>
    @include('layouts.menu')
    @yield('body')
</body>
</html>