@extends('layouts.layout')
@section('title')
create new product
@stop

@section('body')
    {!! Form::open(['route' => 'product.store'])!!}

    {!! Form::label('name','Name')!!}
    {!! Form::text('name', null , ['placeholder' => "give a name"])!!}

    <br>

    {!! Form::label('price','Price')!!}
    {!! Form::text('price', "0$" , ['placeholder' => "give a price"])!!}

    {!! Form::submit('Create')!!}

    {!! Form::close() !!}
@stop
