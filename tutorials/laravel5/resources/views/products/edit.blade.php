@extends('layouts.layout')
@section('title')
Edit {{$product->name}}
@stop

@section('body')
  {!! Form::model($product,[
    'method' => 'patch',
    'route' => ['product.update', $product ->id]
    ]) !!}

    {!! Form::label('name','Name')!!}
    {!! Form::text('name', $product->name , ['placeholder' => "give a name"])!!}

    <br>

    {!! Form::label('price','Price')!!}
    {!! Form::text('price', $product->price , ['placeholder' => "give a price"])!!}

  {!! Form::submit('Edit') !!}
  {!! Form::close()!!}
@stop
