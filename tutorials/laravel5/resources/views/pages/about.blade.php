@extends('layouts.layout')
@section('title')
    About
@stop

@section('body')
    <h1>this is the about page.</h1>
    
    <p>{{$companyname}}</p>
    
    @if($isUserRegistered == true)
        <p>hello mate!</p>
    @else
        <p>please register!</p>
    @endif
    
    @for ($i = 0; $i < 10; $i++)
        <p>The current value is {{ $i }}</p>
    @endfor
    
    @foreach($users as $data)
        {{$data}}<br>
    @endforeach
@stop


